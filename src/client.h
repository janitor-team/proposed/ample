#define BYTESBETWEENMETA 16000
#define NETBUFFSIZE 4000

struct client_config {
	unsigned short int mode;
	char * username;
	char * password;
	char * requestpath;
	long startpos;
	long endpos;
	FILE *stream;
	int statussock;
	mp3entry *mp3base;
	bool metadata;
};

extern struct client_config *cconf;

/*********************************************************
 REMOVED from config - global?
 *********************************************************

	int port;
	bool inetd;
	bool order;
	bool trace;
	int debuglevel;
	int max_clients;
	char * username;
	char * password;
	char **pathlist;
	char * logfile;
	char * conffile;
	char * htmlfile;
	char * htmlheader;
	char * htmlmiddle;
	char * htmlfooter;
	char * servername;
	char * serveraddress;	




**********************************************************/
#define SHOUTSERVMSG "ICY 200 OK\r\n\
icy-notice1:This stream is served using Ample/%s\r\n\
icy-notice2:AMPLE - An MP3 LEnder - http://ample.sf.net/\r\n\
icy-name:%s\r\n\
icy-genre:Mixed\r\n\
icy-url:http://ample.sf.net/\r\n\
icy-pub:1\r\n\
icy-metaint:%d\r\n\
icy-br:128\r\n\
\r\n"

#define SINGLESERVMSG "HTTP/1.1 200 OK\r\n\
Server: Ample/%s\r\n\
Accept-Range: bytes\r\n\
Content-Type: audio/mpeg\r\n\
Content-Length: %lu\r\n\
Connection: close\r\n\
\r\n"

#define PARTIALSERVMSG "HTTP/1.1 206 Partial Content\r\n\
Server: Ample/%s\r\n\
Accept-Range: bytes\r\n\
Content-Type: audio/mpeg\r\n\
Content-Range: bytes %lu-%lu/%lu\r\n\
Connection: close\r\n\
\r\n"

#define BASICSERVMSG "HTTP/1.1 200 OK\r\n\
Server: Ample/%s\r\n\
Accept-Range: none\r\n\
Content-Type: audio/mpeg\r\n\
Connection: close\r\n\
\r\n"

#define HTTPSERVMSG "HTTP/1.1 200 OK\r\n\
Server: Ample/%s\r\n\
Accept-Range: none\r\n\
Content-Type: text/html\r\n\
Connection: close\r\n\
\r\n"

#define M3USERVMSG "HTTP/1.1 200 OK\r\n\
Server: Ample/%s\r\n\
Accept-Range: none\r\n\
Content-Type: audio/x-mpegurl\r\n\
Connection: close\r\n\
\r\n"

#define AUTHMSG "HTTP/1.1 401 Unauthorized\r\n\
WWW-authenticate: Basic realm=\"%s\"\r\n\
\r\n"

#define INFOHEADER "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n\
<html><head>\n\
<title>%s</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\n\
</head><body><center>\n\
<p><h1>%s</h1></p>\n\
<p><h2>Information page</h2></p>\n\
<p>&nbsp;</p>\n\
<div align=\"center\"><table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" bordercolor=\"#000000\">"

#define INFOFOOTER "</table></div>\n\
<p align=\"right\"><font size=\"-1\">powered by Ample, for more information, see the\n\
<a href=\"http://ample.sourceforge.net\">project homepage</a></font></p>\n\
</center></body></html>"

extern int handleclient(int conn, int udpsock);
