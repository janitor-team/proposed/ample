/* Default path */
#ifndef DEF_PATH
#define DEF_PATH "/usr/local/shared/mp3"
#endif
/* Default logfile */
#ifndef DEF_LOGFILE
#define DEF_LOGFILE "/usr/local/var/log/ample"
#endif
/* Default configfile */
#ifndef DEF_CONFFILE
#define DEF_CONFFILE "/usr/local/etc/ample.conf"
#endif
/* Default HTMLtemplate */
#ifndef DEF_HTMLFILE
#define DEF_HTMLFILE "/usr/local/etc/ample.html"
#endif

#define DEF_PORT 1234           /* Port to listen to */
#define DEF_MAX_CLIENTS 5       /* Max simultaneous clients */
#define DEF_SERVERNAME "Ample"  /* Default server name */
#define DEF_SERVERADDRESS "127.0.0.1" /* Default address if gethostbyname fails */
#define LINELENGTH 400          /* Size of buffer used with fgets() */

#define DEF_HTMLHEADER "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n\
<html><head>\n\
<title>@SERVERNAME@</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\n\
</head><body><center>\n\
<p><h1>@SERVERNAME@</h1></p>\n\
<p><h2>Tracks currently available in @PATH@</h2></p>\n\
<p>&nbsp;</p><p><font size=\"-1\">[ <a href=\"index.m3u\">playlist for this dir</a> | <a href=\"rindex.m3u\">recursive playlist</a> ]</font></p>\n\
<p><font size=\"-1\">[ <a href=\"../index.html\">Up one level</a> ]</font></p>\n\
<div align=\"center\"><table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" bordercolor=\"#000000\">\n\
<tr>\n<td><b>TYPE</b></td><td><b>URL</b></td></tr>\n"
#define DEF_HTMLMIDDLE "<tr><td>@TYPE@</td><td><a href=\"@URL@\">@NAME@</a></td></tr>\n"
#define DEF_HTMLFOOTER "</table></div>\n\
<p align=\"right\"><font size=\"-1\">powered by Ample, for more information, see the\n\
<a href=\"http://ample.sourceforge.net\">project homepage</a></font></p>\n</center></body></html>\r\n"

/* Config file option types */
#define OPT_BOOL 1              /* Boolean option                 */
#define OPT_INT 2               /* Integer option                 */
#define OPT_STRING 3            /* String option                  */
#define OPT_MULTISTRING 4       /* String option with > 1 strings */
#define OPT_FLOAT 5             /* Float option                   */

#define CONN_TIMEOUT 60         /* The time a client has to complete the http request */


#define	USAGE1 "Usage: %s [OPTION(S)]... [PATH]\n\
\n\
AMPLE - An MP3 LEnder\n\
recursively indexes all MP3 files it finds below PATH and then listens\n\
to a TCP port for incoming connections. Once a connection is made AMPLE will\n\
start sending randomly chosen MP3's. If called from the command line or a\n\
script AMPLE will go into daemon mode (unlike when called from inetd or with\n\
the option -t).\n\
\n"

#if HAVE_GETOPT_H
#define USAGE2 "\
  -p, --port=NUMBER           which port to listen to, default %d\n\
  -o, --order                 play MP3 files in alphabetical order\n\
  -c, --clients=NUMBER        how many clients are allowed to be connected\n\
                              default %d\n\
  -n, --norecursive           don't index MP3 files in subdirs of the given dir\n\
  -f, --conffile=FILENAME     alternative file to read for config options\n\
  -m, --htmlfile=FILENAME     file to use as template for HTML output\n\
  -h, --help                  display this help and exit\n\
  -d, --debug[=NUMBER]        debug messages will be printed\n\
                              higher number means more detail\n\
  -t, --trace                 no forking, no backgrounding\n\
                              helpful when debugging\n\
  -v, --version               output version information and exit\n\
\n\
Report bugs to <david@2gen.com>\n"
#else
#define USAGE2 "\
  -p=NUMBER                   which port to listen to, default %d\n\
  -o                          play MP3 files in alphabetical order\n\
  -c=NUMBER                   how many clients are allowed to be connected,\n\
                              default %d\n\
  -n                          don't index MP3 files in subdirs of the given dir\n\
  -f=FILENAME                 alternative file to read for config options\n\
  -m=FILENAME                 file to use as template for HTML output\n\
  -h                          display this help and exit\n\
  -d[=NUMBER]                 debug messages will be printed,\n\
                              higher number means more detail\n\
  -t                          no forking, no backgrounding,\n\
                              helpful when debugging)\n\
  -v                          output version information and exit\n\
\n\
Report bugs to <david@2gen.com>\n"
#endif

/* Reads and parses client http request */
extern bool readrequest (struct client_config *cconf);

/* Checks command line, config file and default options then set conf */
extern void checkopt(int argc, char * argv[]);

/* Struct used when parsing config files */
struct confoption {
	char *name;
	unsigned short int type;
	void *value;
};
